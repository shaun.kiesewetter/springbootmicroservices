package net.wedocode.security.util

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service
import java.util.*
import kotlin.reflect.KFunction1

@Service
class JwtUtil {
    private val SECRET_KEY = "secret"

    fun extractUserName(token: String): String? = extractClaim(token, Claims::getSubject) as String?

    fun extractExpiration(token: String): Date = extractClaim(token, Claims::getExpiration) as Date

    fun isTokenExpired(token: String): Boolean = extractExpiration(token).before(Date())

    fun generateToken(userDetails: UserDetails) : String {
        val claims = hashMapOf<String, Any>()
        return createToken(claims, userDetails.username).toString()
    }

    fun createToken(claims: Map<String, Any>, subject: String): String? {
        return Jwts.builder()
            .setClaims(claims)
            .setSubject(subject)
            .setIssuedAt(Date(System.currentTimeMillis()))
            .setExpiration(Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10))
            .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
            .compact()
    }

    private fun extractClaim(token: String, claimsResolver: KFunction1<Claims, Any>): Any {
        val claims = extractAllClaims(token)
        return claimsResolver.call(claims)
    }


    private fun extractAllClaims(token: String): Claims {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).body
    }

    fun validateToken(token: String, userDetails: UserDetails): Boolean{
        val userName = extractUserName(token)
        return ((userName == userDetails.username) &&
                (!isTokenExpired(token)))
    }


}


