package net.wedocode.security.services

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service

@Service
class MyUserDetailsService: UserDetailsService {
    override fun loadUserByUsername(username: String?): UserDetails {
        // Hard coded for now but should be loaded from an identity store like JPA or even LDAP
        return User("Shaun", "Shaun", arrayListOf<GrantedAuthority>())
    }
}
