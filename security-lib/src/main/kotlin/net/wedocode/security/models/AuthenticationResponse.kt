package net.wedocode.security.models

data class AuthenticationResponse(val jwtToken: String) {
}