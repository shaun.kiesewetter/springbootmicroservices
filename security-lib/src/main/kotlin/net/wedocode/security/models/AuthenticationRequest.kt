package net.wedocode.security.models

data class AuthenticationRequest(val username: String, val password: String) {
}