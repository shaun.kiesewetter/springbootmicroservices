package net.wedocode.consumerService.models

data class AuthenticationRequest(val username: String, val password: String) {
}