package net.wedocode.consumerService.models

data class AuthenticationResponse(val jwtToken: String) {
}