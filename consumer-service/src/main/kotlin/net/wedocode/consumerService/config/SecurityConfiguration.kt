package net.wedocode.consumerService.config

import net.wedocode.consumerService.filters.JwtRequestFilter
import net.wedocode.consumerService.services.MyUserDetailsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.password.NoOpPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@EnableWebSecurity
class SecurityConfiguration : WebSecurityConfigurerAdapter() {
    @Autowired
    lateinit var userDetailsService: MyUserDetailsService

    @Autowired
    lateinit var jwtRequestFilter: JwtRequestFilter
    // Authentication configuration add some in memory users

    //override fun configure(auth: AuthenticationManagerBuilder?) {
    //    auth?.inMemoryAuthentication()
    //        ?.withUser("Shaun")?.password("Shaun")?.roles("ADMIN")?.and()
    //        ?.withUser("Jack")?.password("Jack")?.roles("USER")
    //}

    // Use a user details server
    override fun configure(auth: AuthenticationManagerBuilder?) {
        auth?.userDetailsService(userDetailsService)
    }

    // Authorization
    override fun configure(http: HttpSecurity?) {

        http?.csrf()?.disable()
            ?.authorizeRequests()
            ?.antMatchers("/authenticate")
            ?.permitAll()
            ?.anyRequest()
            ?.authenticated()
            ?.and()
            ?.sessionManagement()
            ?.sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        http?.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter::class.java)

//        http?.authorizeRequests()
//            ?.antMatchers("/admin")?.hasRole("ADMIN")
//            ?.antMatchers("/user")?.hasAnyRole("USER", "ADMIN")
//            ?.antMatchers("/")?.permitAll()
//            ?.and()?.formLogin()
    }

    @Bean
    fun getPasswordEncoder(): PasswordEncoder{
        return NoOpPasswordEncoder.getInstance()
    }

    @Override
    @Bean
    override fun authenticationManagerBean(): AuthenticationManager{
        return super.authenticationManagerBean()
    }
}