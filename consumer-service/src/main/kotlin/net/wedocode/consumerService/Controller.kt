package net.wedocode.consumerService

import net.wedocode.consumerService.models.AuthenticationRequest
import net.wedocode.consumerService.models.AuthenticationResponse
import net.wedocode.consumerService.services.MyUserDetailsService
import net.wedocode.consumerService.util.JwtUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.boot.web.client.RestTemplateCustomizer
import org.springframework.http.HttpRequest
import org.springframework.http.ResponseEntity
import org.springframework.http.client.ClientHttpRequestExecution
import org.springframework.http.client.ClientHttpRequestInterceptor
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate

@RestController
class Controller {

    @Autowired
    lateinit var restTemplate: RestTemplate

    @Autowired
    lateinit var authenticationManager: AuthenticationManager

    @Autowired
    lateinit var userDetailsService: MyUserDetailsService

    @Autowired
    lateinit var jwtUtil: JwtUtil

    @GetMapping
    fun getGreetingFromConfigDemo(): String{


        val response =  restTemplate.getForEntity("http://greetingService/greeting", String::class.java)


        println(response.statusCode)
        println(response.body)

        return response.body.toString()
    }

    @GetMapping("/admin")
    fun admin(@RequestHeader("Authorization") authorization: String?): String {

        var jwt: String? = null

        // Demonstrates flowing the JWT onwards to down stream greeting service
        if (authorization != null && authorization.startsWith("Bearer "))
            jwt = authorization.substring(7)

        // Inject the incoming jwt token in the request header
        val restTemplateWithToken = RestTemplateBuilder(RestTemplateCustomizer { rt: RestTemplate ->
            rt.interceptors.add(
                ClientHttpRequestInterceptor { request: HttpRequest, body: ByteArray?, execution: ClientHttpRequestExecution ->
                    request.headers.add("Authorization", "Bearer $jwt")
                    execution.execute(request, body!!)
                })
        }).build()


        val response =  restTemplateWithToken.getForEntity("http://greetingService/greeting", String::class.java)
        return "<h1>Welcome Admin</h1> $response"
    }

    @GetMapping("/user")
    fun user(): String {
        val userName = (SecurityContextHolder.getContext().authentication.principal as UserDetails).username
        return "<h1>Welcome $userName</h1>"
    }

    @RequestMapping("/authenticate", method = arrayOf(RequestMethod.POST))
    fun createAuthenticationToken(@RequestBody authenticationRequest: AuthenticationRequest): ResponseEntity<AuthenticationResponse>{
        try {
            authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(
                    authenticationRequest.username,
                    authenticationRequest.password
                )
            )
        } catch (e: BadCredentialsException){
            throw Exception("Incorrect username or password entered")
        }

        val userDetails = userDetailsService.loadUserByUsername(authenticationRequest.username)
        val jwtToken = jwtUtil.generateToken(userDetails)
        return ResponseEntity.ok(AuthenticationResponse(jwtToken))
    }
}