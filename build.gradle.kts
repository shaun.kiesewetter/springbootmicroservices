//import org.jmailen.gradle.kotlinter.tasks.LintTask
//import org.jetbrains.kotlin.konan.properties.Properties

plugins {
//    id("org.jmailen.kotlinter")
//    id("com.adarshr.test-logger")
//    kotlin("jvm")
}

//tasks.check {
//    dependsOn(tasks.installKotlinterPrePushHook)
//}

/**
 * The following dependencies are only needed for money-out bank connectors.
 * They will be removed once the project is no longer a monorepo
 **/
//dependencies {
//    implementation("com.squareup.moshi:moshi:1.12.0")
//    implementation("com.squareup.moshi:moshi-adapters:1.12.0")
//    implementation("com.squareup.moshi:moshi-kotlin:1.12.0")
//// https://mvnrepository.com/artifact/com.squareup.okhttp3/okhttp
//    implementation("com.squareup.okhttp3:okhttp:4.9.1")
//    implementation("org.springframework.boot:spring-boot-starter-test:2.5.6")
//}

//fun fromLocalSettings(key: String): String? {
//    properties.load(localPropsFileStream)
//    return properties.getProperty(key)
//}


// Source the Artifactory build variables from the local.properties file if it exists on the project root.
//val properties = Properties()
//val haslocalProperties = project.file("local.properties").exists()
//val localPropsFileStream = if (haslocalProperties) project.file("local.properties").inputStream() else null
//val artifactoryUser = if (haslocalProperties) fromLocalSettings("artifactoryUser") else System.getenv("artifactoryUser")
//val artifactoryPassword = if (haslocalProperties) fromLocalSettings("artifactoryPassword") else System.getenv("artifactoryPassword")
//val artifactoryRepoKey = if (haslocalProperties) fromLocalSettings("artifactoryRepoKey") else System.getenv("artifactoryRepoKey")
//
//allprojects {
//    apply(plugin = "org.jmailen.kotlinter")
//    apply(plugin = "com.adarshr.test-logger")
//    apply(plugin = "org.jetbrains.kotlin.jvm")
//
//    java.sourceCompatibility = JavaVersion.VERSION_11
//
//    repositories {
//        mavenCentral()
//        maven {
//            setUrl("https://yocotechnologies.jfrog.io/artifactory/${artifactoryRepoKey}")
//            credentials {
//                username = artifactoryUser
//                password = artifactoryPassword
//            }
//        }
//    }
//
//    configurations {
//        all {
//            exclude("org.springframework.boot", "spring-boot-starter-logging")
//        }
//    }
//
//    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
//        dependsOn(tasks.formatKotlin)
//        dependsOn(tasks.lintKotlin)
//
//        kotlinOptions {
//            freeCompilerArgs = listOf("-Xjsr305=strict")
//            jvmTarget = "11"
//        }
//    }
//
//    tasks {
//        "lintKotlinMain"(LintTask::class) {
//            exclude("$rootDir/money-out/bank-connectors/absa-za/build/gen/openapi")
//        }
//    }

    tasks.withType<Test> {
        useJUnitPlatform()
    }

    // Run gradle -q PrintArtifactoryVariables from terminal to see where variables are being sourced from and their values.
//    tasks.register<PrintArtifactVariables>("PrintArtifactoryVariables"){
//        variables = listOf(
//            "Artifactory User: $artifactoryUser\n",
//            "Artifactory Repo Key: $artifactoryRepoKey\n",
//            "Variables sourced from local? : $haslocalProperties"
//        )
//    }
//}

//abstract class PrintArtifactVariables(): DefaultTask(){
//    @Input
//    var variables: List<String> = listOf("")
//
//    @TaskAction
//    fun showSourcedVariables(){
//        variables.forEach{System.out.print(it)}
//    }
//}

//testlogger {
//    theme = com.adarshr.gradle.testlogger.theme.ThemeType.STANDARD
//    showExceptions = true
//    showStackTraces = true
//    showFullStackTraces = false
//    showCauses = true
//    slowThreshold = 2000
//    showSummary = true
//    showSimpleNames = false
//    showPassed = true
//    showSkipped = true
//    showFailed = true
//    showStandardStreams = false
//    showPassedStandardStreams = true
//    showSkippedStandardStreams = true
//    showFailedStandardStreams = true
//    logLevel = LogLevel.LIFECYCLE
//}
