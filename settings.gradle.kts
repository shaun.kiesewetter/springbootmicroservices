rootProject.name = "hack-week"

// This section is strictly for version control
pluginManagement {
//    plugins {
//        id("io.spring.dependency-management") version "1.0.11.RELEASE"
//        id("org.springframework.boot") version "2.5.4"
//        id("org.jmailen.kotlinter") version "3.6.0"
//        id("com.adarshr.test-logger") version "3.0.0"
//        kotlin("jvm") version "1.5.21"
//        kotlin("plugin.spring") version "1.5.21"
//    }
}

include(
    "discovery-server",
    "greetingService",
    "consumer-service",
    "spring-cloud-config-server",
    "security-lib"

)
