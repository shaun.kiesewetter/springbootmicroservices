package net.wedocode.greetingsService

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController


@RestController
class GreetingServiceController {
    @Value("\${my.greeting}")
    lateinit var myGreeting: String

    @Autowired
    private val webserverApplicationContext: ServletWebServerApplicationContext? = null


    @GetMapping("/greeting")
    fun greeting(): String {
        val myCurrentProcessId = System.getProperty("PID")
        val myCurrentPort = webserverApplicationContext?.webServer?.port

        return """
            This service is currently being load balanced you are receiving this response from: ${"\n"}
            
            This request has been served from process id: $myCurrentProcessId \n
            Running under port: $myCurrentPort \n 
            
            Server Response message from configuration is: $myGreeting
        """


    }

}